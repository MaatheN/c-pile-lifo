//
//  pile.h
//  pile
//
//  Created by Tatadmound on 22/04/2021.
//

#ifndef pile_h
#define pile_h
typedef struct Pile Pile;
struct Pile {
    PileElement *firstElement;
    int pileLength;
};

#endif /* pile_h */
