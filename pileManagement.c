//
//  pileManagement.c
//  pile
//
//  Created by Tatadmound on 22/04/2021.
//

#include "pileManagement.h"
#include <stdlib.h>
Pile* initStack(){
    Pile* stack = malloc(sizeof(Pile));
    stack->firstElement = NULL;
    stack->pileLength=0;
    
    return stack;
}

char pushElement(Pile *stack, int value){
    //1) set the element's values
    PileElement *newPileElementPointer = malloc(sizeof(PileElement));
    if (stack == NULL || newPileElementPointer == NULL) {
        exit(EXIT_FAILURE);
    }
    newPileElementPointer->value = value;
        //his next value is the current .first element (even if its NULL)
    newPileElementPointer->nextElement = stack->firstElement;
    //2) update .first of the stack
    stack->firstElement = newPileElementPointer;
    //4) update pileLength
    stack->pileLength++;
    return 0;
}

int popElement(Pile *stack){
    if (stack == NULL) {
        exit(EXIT_FAILURE);
    }
    PileElement *elementToDelete = stack->firstElement;
    int elementToDeleteValue = 0;
    if (stack !=NULL && elementToDelete != NULL) {
        elementToDeleteValue = elementToDelete->value;
        //update .firstElement
        stack->firstElement = stack->firstElement->nextElement;
        //free the first element
        free(elementToDelete);
    }
    return elementToDeleteValue;
}

char showStack(Pile *stack){
    //1) read the first element
    PileElement *currentStackElementPointer = malloc(sizeof(PileElement));
    currentStackElementPointer = stack->firstElement;
    //2) check if it is null, if it is -> 5)
    if (currentStackElementPointer == NULL || stack == NULL) {
        exit(EXIT_FAILURE);
    }
    while (currentStackElementPointer!=NULL) {
        //3) print his .value
        printf("\n%d\n",currentStackElementPointer->value);
        //4) next element -> 2)
        currentStackElementPointer=currentStackElementPointer->nextElement;
    }
    //5) print "end of prompt"
    printf("\nFin de l'affichage de la pile\n");
    return 0;
}
