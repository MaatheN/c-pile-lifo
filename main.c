//
//  main.c
//  pile
//
//  Created by Tatadmound on 22/04/2021.
//

#include <stdio.h>
#include <stdlib.h>
#include "pileManagement.h"

int main(int argc, const char * argv[]) {
    Pile *stack = initStack();
    pushElement(stack, 12);
    pushElement(stack, 24);
    pushElement(stack, 4);
    pushElement(stack, 7);
    
    showStack(stack);
    
    popElement(stack);
    popElement(stack);
    
    showStack(stack);
    
    return 0;
}
