//
//  pileManagement.h
//  pile
//
//  Created by Tatadmound on 22/04/2021.
//

#ifndef pileManagement_h
#define pileManagement_h

#include <stdio.h>
#include "pileElement.h"
#include "pile.h"
//initialisation d'une pile
Pile* initStack(void);
//empillage d'un élément
//pushing an element on top of the stack
char pushElement(Pile *stack, int value);
//dépillage d'un élément
//popping an element from the top of the stack
int popElement(Pile *stack);
char showStack(Pile *stack);
#endif /* pileManagement_h */
