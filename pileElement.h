//
//  pileElement.h
//  pile
//
//  Created by Tatadmound on 22/04/2021.
//

#ifndef pileElement_h
#define pileElement_h
typedef struct PileElement PileElement;
struct PileElement{
    int value;
    PileElement *nextElement;
};

#endif /* pileElement_h */
